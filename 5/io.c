//
// Created by dunaev on 06.10.17.
//

#include "io.h"
#include "err.h"
#include <stdlib.h>
#include <string.h>

void flush_input(void) {
	char c;
	while (scanf("%c", &c) == 1 && c != '\n');
}

errors_t read_in_buffer() {
	if (fgets(buffer, BUFFER_SIZE, stdin) == NULL) {
		return INVALID;
	}
	if (strlen(buffer) >= BUFFER_SIZE - 2) {
		ungetc('1', stdin);
		flush_input();
		return TOBIG;
	}
	return CONTINUE;
}

errors_t get_answer_from_buffer(char *buf, long *X) {
	buf = skip_space(buf);
	char *end;
	*X = strtol(buf, &end, 10);
	if (buf == end)
		return NOT_A_NUM;
	if (skip_space(end) == end && *end != '\n')
		return NOT_A_NUM;
	if (*X <= 0)
		return UNEXPECTED;
	return CONTINUE;
}

char *skip_space(char *start) {
	int i = 0;
	while ((start[i] == ' ' || start[i] == '\t' || start[i] == '\v' || start[i] == '\f') && start[i] != '\0') {
		i++;
	}
	return &start[i];
}