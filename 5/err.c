//
// Created by dunaev on 06.10.17.
//

#include "err.h"
#include <stdio.h>

const char *errors_arr[] = {
		"",
		"Incorrect command, please try again!",
		"Expression is too big",
		"Invalid operation! Parameter is not number",
		"Invalid operation! to many parameters",
		"Unexpected Error",
		"Unexpected Error, try go out",
		"Leaf does not exist",
		"",
};

void say_error(errors_t err) {
	printf("%s\n", errors_arr[err]);
}
