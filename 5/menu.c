//
// Created by dunaev on 06.10.17.
//
#include "err.h"
#include "menu.h"
#include <stdio.h>

struct _menu_t {
	int level;
	int down_level;
	char name[32];
	errors_t (*callback)();
};

//struct _menu_t {
//	menu_t *second;
//	menu_t *sub_menu;
//	char name[32];
//	errors_t (*callback)();
//};

errors_t File () {
	printf("TODO File ");
	return CONTINUE;
}

errors_t Load () {
	printf("TODO Load ");
	return CONTINUE;
}

errors_t Save () {
	printf("TODO Save ");
	return CONTINUE;
}

errors_t Change () {
	printf("TODO Change ");
	return CONTINUE;
}

errors_t Add () {
	printf("TODO Add ");
	return CONTINUE;
}

errors_t Delete () {
	printf("TODO Delete ");
	return CONTINUE;
}

errors_t Find () {
	printf("TODO Find ");
	return CONTINUE;
}

errors_t Done () {
	printf("TODO Done ");
	return CONTINUE;
}

const menu_t menu[] = {
	{.level = 0, .down_level = 1, .name = "File", .callback = File},
		{.level = 1, .down_level = 0, .name = "Load", .callback = Load},
		{.level = 1, .down_level = 0, .name = "Save", .callback = Save},
	{.level = 0, .down_level = 2, .name = "Change", .callback = Change},
		{.level = 2, .down_level = 0, .name = "Add", .callback = Add},
		{.level = 2, .down_level = 0, .name = "Delete", .callback = Delete},
	{.level = 0, .down_level = 0, .name = "Find", .callback = Find},
	{.level = 0, .down_level = 0, .name = "Done", .callback = Done},
};

menu_t *work_with_menu(int level, int find) {
	int index = 0;
	for (int i = 0; i < sizeof(menu) / sizeof(menu_t); ++i) {
		if (menu[i].level == level) {
			index++;
			if (find) {
				if (find == index) {
					return &menu[i];
				}
			} else
				printf("%d)\t%s\n", index, menu[i].name);
		}
	}
	return menu;
}

void call_menu_option() {
	int level = 0;
	int index;
	work_with_menu(level, 0);

//	err = read_in_buffer();
//	if (err != CONTINUE)
//		return err;
//	err = get_answer_from_buffer(buffer, &height);
//	if (err != CONTINUE)
//		return err;

}
