#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define MAX_NUM 999999999
#define MAX_NEW_LINE 30

char *errors_arr[] = {
		".",
		"Should be greater than zero",
		"You number is too big",
		"It is not number",
		"Parameters are incompatible",
		"Unexpected Error",
		"Unexpected Error, try go out"
};

enum Errors {
	CORRECT = 0,
	NOTZERO,
	TOBIG,
	NOTNUM,
	UNUSUALARGUMENT,
	IDONTKNOW,
	GO_OUT
};

enum States {
	START = 0,
	COMPUTE,
	EXIT,
	INCORRECT
};

void flush_input(void) {
	char c;
	while (scanf("%c", &c) == 1 && c != '\n');
}

double foo(double a, double r) {
	if (a > -r && a < r) {
		return sqrt(r * r - a * a);
	}
	if (a >= r && a <= r + 5) {
		return a * r / 5 - r * r / 5;
	}
	if (a >= r + 5) {
		return r;
	}

	if (a <= -r && a >= -r - 2) {
		return a + r;
	}

	if (a < -r - 2) {
		return -3;
	}
	return -1;// never executed
}

void say_error_on_parametr(enum Errors err) {
	printf("Wrong input parameter, %s.\n", errors_arr[err]);
	printf("Type ‘y’ for repeat, or any other for exit from application… \n");
}

char get_byte_answer() {
	char readChar = (char) getchar();
	char nextChar = '\n';
	if ('\n' != readChar) {
		nextChar = (char) getchar();
	}
	if ('\n' != nextChar) {
		flush_input();
		return 0;
	}
	return readChar;
}

enum Errors get_answer_on_error(char *correct_symbols) {
	char readChar = get_byte_answer();
	if (strchr(correct_symbols, readChar)== NULL || readChar == 0) {
		return GO_OUT;
	}
	printf("\n");
	return IDONTKNOW;
}

enum Errors get_float(double *X) {
	long err = 0;
	err = scanf("%lf", X);//TODO strtod()

	char nextChar = (char) getchar();

	if ('\n' != nextChar) {
		flush_input();
	}

	if (err < 1 || nextChar != '\n') {
		return NOTNUM;
	}
	if (*X < -MAX_NUM || *X > MAX_NUM)
	{
		return TOBIG;
	}
	return CORRECT;
}

//TODO auto change width
void solve_task(double Xstart, double Xend, double dX, int r) {
	printf("┌───────────────┬───────────────┐\n");
	printf("│       X       │       Y       │\n");
	printf("├───────────────┼───────────────┤\n");
	int i = 0;
	for (double a = Xstart; a < Xend; a += dX) {
		double tmp = foo(a, r);
		printf("│% 14.3f │% 14.3f │\n", a, tmp);
		i++;
	}
	printf("└───────────────┴───────────────┘\n");
}

enum Errors get_value(char *query, double *value, int check_zero) {
	enum Errors err;
	do {
		printf("%s", query);
		err = get_float(value);
		if (CORRECT == err && *value < 0.0005 && check_zero) {
			err = NOTZERO;
		}
		if (CORRECT != err) {
			say_error_on_parametr(err);
			err = get_answer_on_error("yY");
		}
	} while (!(CORRECT == err || GO_OUT == err));
	return err;
}

int main() {
	printf("Dulfilled Alexey Dunaev, P3117; 6 option\n");

	enum States state = START;
	double radius = 0;
	double Xstart;
	double Xend;
	double dX;
	char readChar;
	enum Errors err = UNUSUALARGUMENT;

	while (state != EXIT) {
		switch (state) {
			case START:
				printf("1. Compute a function\n");
				printf("2. Exit\n");
				printf("Choose a menu item: ");

				readChar = get_byte_answer();
				if (!strchr("12", readChar) || readChar == 0) {
					printf("\nPlease, choose a correct menu item:\n");
					break;
				}
				state = (enum States) (readChar - '0');
				break;
			case COMPUTE:
				err = get_value("Input Radius value: ", &radius, 1);
				if (err == GO_OUT) {
					state = START;
					break;
				}
				err = get_value("Input X start value: ", &Xstart, 0);
				if (err == GO_OUT) {
					state = START;
					break;
				}
				err = get_value("Input X end value: ", &Xend, 0);
				if (err == GO_OUT) {
					state = START;
					break;
				}
				err = get_value("Input delta X value: ", &dX, 1);
				if (err == GO_OUT) {
					state = START;
					break;
				}

				if (Xend < Xstart || (Xend - Xstart)/dX < 1) {
					err = UNUSUALARGUMENT;
					state = INCORRECT;
					break;
				}

				if ((Xend - Xstart)/dX > MAX_NEW_LINE) {
					err = TOBIG;
					state = INCORRECT;
					break;
				}

				solve_task(Xstart, Xend, dX, 3);

				state = EXIT;
				break;

			case INCORRECT:
				say_error_on_parametr(err);
				err = get_answer_on_error("yY");

				if (err == GO_OUT) {
					state = START;
					printf("\n");
					break;
				}

				state = COMPUTE;
				break;

			default:
				state = EXIT;
		}
	}

	return 0;
}
