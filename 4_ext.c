//
// Created by dunaev on 21.10.17.
//

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>

int main(int argc, char const *argv[]) {
	if (argc <= 1) {
		printf("Please provide the name of file.\n");
		return 1;
	}

	if (argc > 2) {
		printf("Wrong arguments for application. Too many args.\n");
		return 2;
	}

	FILE *file;
	file = fopen(argv[1], "r");

	if (file == NULL) {
		if (errno == EACCES) {
			printf("File “%s” can’t be read. Permission denied\n", argv[1]);
			return 4;
		}
//		if errno == ENOENT
		printf("File “%s” not found. File does not exist\n", argv[1]);
		return 3;
	}

	fclose(file);

	system("../../build/bin/tomita-parser config.proto");

	return 0;
}