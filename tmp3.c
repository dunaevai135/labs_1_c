#include <stdio.h>
#include <string.h>

void flush_input(void) {
	ungetc('\n', stdin);
	char c;
	while ( scanf("%c", &c) == 1 && c != '\n' );
}


char get_byte_answer() {
	char readChar = (char) getchar();
	char nextChar = '\n';
	if ('\n' != readChar) {
		nextChar = (char) getchar();
	}
	if ('\n' != nextChar) {
		flush_input();
		return -1;
	}
	return readChar;
}


int main(int argc, char const *argv[])
{
	char choice;
	for (int i = 0; i < 10; ++i) {
		do {
			printf ("1: Check spelling\n");
			printf ("2: Correct spelling\n");
			printf ("3: Look up a word in the dictionary\n");
			printf("4: Quit\n");
			printf("\nEnter your selection: ");
			choice = getchar();
		} while(!strchr ("1234", choice));
		// choice = '\n';
		printf("%d\n", choice);
		flush_input();
	}
	return 0;
}
