//
// Created by dunaev on 28.09.17.
//

#include <stdio.h>
#include <stdint.h>

struct car {
	int32_t id;
	long pos;
	int32_t color;
	int32_t power;
	int32_t month;
	char number[11];
	char name[60];
	struct car *second;
	struct car *previous;
};


int main(int argc, char const *argv[]) {
	if (argc <= 1) {
		printf("Please provide the name of file.\n");
		return 1;
	}

	if (argc > 2) {
		printf("Wrong arguments for application. Too many args.\n");
		return 2;
	}

	FILE *file;
	file = fopen(argv[1], "wb");

	if (file == NULL) {
		printf("File “%s” not found.\n", argv[1]);
		return 3;
	}

	int a[] = {2,4,5,6,2,3,5};

	fwrite(a, sizeof(a[0]), sizeof(a) / sizeof(a[0]), file);

	if (ferror(file)) {
		printf("File “%s” can’t be read.\n", argv[1]);
	}

	fclose(file);

	return 0;
}
