//
// Created by dunaev on 11.09.17.
//

#ifndef UNTITLED_TREE_LIB_H
#define UNTITLED_TREE_LIB_H

struct node_t;

struct node_t *add_tree(struct node_t *p, double key, double value);

void free_mem(struct node_t *tree);

void tree_print(struct node_t *p);

double find_by_key(struct node_t *p, double key);

struct node_t* balance(struct node_t* p);

#endif //UNTITLED_TREE_LIB_H
