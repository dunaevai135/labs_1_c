#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tree_lib.h"

#define EXP 1e-9

struct node_t {
	double key;
	double value;
	int count;
	unsigned char height;
	struct node_t *left;
	struct node_t *right;
};

struct node_t *add_tree(struct node_t *p, double key, double value) {
	if (p == NULL) {
		p = (struct node_t *) malloc(sizeof(struct node_t));
		p->key = key;
		p->value = value;
		p->count = 1;
		p->height = 1;
		p->left = p->right = NULL;
	} else if (fabs(key - p->key) < EXP)
		p->count++;
	else if (key < p->key)
		p->left = add_tree(p->left, key, value);
	else
		p->right = add_tree(p->right, key, value);
	return balance(p);
}

void free_mem(struct node_t *tree) {
	if (tree != NULL) {
		free_mem(tree->left);
		free_mem(tree->right);
		free(tree);
	}
}

void tree_print(struct node_t *p) {
	if (p != NULL) {
		printf("%0.3f, %0.3f, ", p->key, p->value);
		printf("(");
		tree_print(p->left);
		printf("), (");
		tree_print(p->right);
		printf(")");
	}
}

double find_by_key(struct node_t *p, double key) {
	if (p != NULL) {
		if (fabs(key - p->key) < EXP)
			return p->value;
		else if (key < p->key)
			return find_by_key(p->left, key);
		else
			return find_by_key(p->right, key);
	}
	return NAN;
}

unsigned char height(struct node_t *p) {
	return p ? p->height : 0;
}

void fixheight(struct node_t *p) {
	unsigned char hl = height(p->left);
	unsigned char hr = height(p->right);
	p->height = (hl > hr ? hl : hr) + 1;
}

int bfactor(struct node_t *p) {
	return height(p->right) - height(p->left);
}

struct node_t *rotateright(struct node_t *p) {
	struct node_t *q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

struct node_t *rotateleft(struct node_t *q) {
	struct node_t *p = q->right;
	q->right = p->left;
	p->left = q;
	fixheight(q);
	fixheight(p);
	return p;
}

struct node_t *balance(struct node_t *p) {
	fixheight(p);
	if (bfactor(p) == 2) {
		if (bfactor(p->right) < 0)
			p->right = rotateright(p->right);
		return rotateleft(p);
	}
	if (bfactor(p) == -2) {
		if (bfactor(p->left) > 0)
			p->left = rotateleft(p->left);
		return rotateright(p);
	}
	return p;
}
