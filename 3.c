//
// Created by dunaev on 11.09.17.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include "tree_lib.h"

#define BUFFER_SIZE 255
#define EXP 1e-9

char buffer[BUFFER_SIZE];

const char *errors_arr[] = {
		"",
		"Incorrect command, please try again!",
		"Expression is too big",
		"Invalid operation! Parameter is not number",
		"Invalid operation! to many parameters",
		"Unexpected Error",
		"Unexpected Error, try go out",
		"Leaf does not exist",
		"",
};

enum Errors {
	CORRECT = 0,
	INVALID,
	TOBIG,
	NOT_A_NUM,
	TOO_MANY_ARGUMENT,
	UNEXPECTED,
	GO_OUT,
	EXIST,
	CONTINUE
};

#define NUM_OF_VALID_COMMANDS 5

const char *valid_commands[NUM_OF_VALID_COMMANDS] = {
		"add ",
		"find ",
		"done",
		"BinaryTree",
		"print",
};

enum States {
	START = 0,
	FIRST,
	SECOND,
	EXIT,
	INCORRECT
};

void flush_input(void) {
	char c;
	while (scanf("%c", &c) == 1 && c != '\n');
}

void say_error(enum Errors err) {
	printf("%s\n", errors_arr[err]);
}

char *skip_space(char *start) {
	int i = 0;
	while ((start[i] == ' ' || start[i] == '\t' || start[i] == '\v' || start[i] == '\f') && start[i] != '\0') {
		i++;
	}
	return &start[i];
}

enum Errors get_f_argument(char **buf, double *X) {
	char *end;
	*buf = skip_space(*buf);
	*X = strtod(*buf, &end);
	if (HUGE_VAL == *X || -HUGE_VAL == *X || *buf == end)
		return NOT_A_NUM;
	*buf = end;
	return CONTINUE;
}

enum Errors get_answer_from_buffer(char *buf, long *X) {
	buf = skip_space(buf);
	char *end;
	*X = strtol(buf, &end, 10);
	if (buf == end)
		return NOT_A_NUM;
	if (skip_space(end) == end && *end != '\n')
		return NOT_A_NUM;
	if (*X <= 0)
		return UNEXPECTED;
	return CONTINUE;
}

enum Errors get_valid_argument(char *buff, long num, double *a, ...) {
	va_list args;
	va_start(args, a);

	enum Errors err;

	err = get_f_argument(&buff, a);
	if (err != CONTINUE) {
		va_end(args);
		return err;
	}

	for (int i = 0; i < num - 1; ++i) {
		err = get_f_argument(&buff, va_arg(args, double*));
		if (err != CONTINUE) {
			va_end(args);
			return err;
		}
	}

	va_end(args);
	if (buff[0] != '\n' && buff[0] != ' ')
		return NOT_A_NUM;
	buff = skip_space(buff);
	if (buff[0] != '\n')
		return TOO_MANY_ARGUMENT;

	return CONTINUE;
}

enum Errors read_in_buffer() {
	if (fgets(buffer, BUFFER_SIZE, stdin) == NULL) {
		return INVALID;
	}
	if (strlen(buffer) >= BUFFER_SIZE - 2) {
		ungetc('1', stdin);
		flush_input();
		return TOBIG;
	}
	return CONTINUE;
}

void get_valid_array(long num, double *arr) {
	for (int k = 0; k < num; ++k) {
		int numOfRead = scanf(" %lf", &arr[k]);
		char nextChar = getchar();
		if (numOfRead < 1 || !(nextChar == ' ' || nextChar == '\n' || nextChar == '\t')) {
			k--;
		}
	}
}

void print_anchor_points(double *arr, long width, long height) {
	long *pmin_w = (long *) calloc(width, sizeof(long));
	long *pmax_h = (long *) calloc(height, sizeof(long));

	for (long i = 0; i < height; ++i) {
		for (long j = 0; j < width; ++j) {
			if (arr[i * width + j] < arr[i * width + pmax_h[i]]) {
				pmax_h[i] = j;
			}
			if (arr[i * width + j] > arr[pmin_w[j] * width + j]) {
				pmin_w[j] = i;
			}
		}
	}

	long flagOf1tAnchor = 1;
	for (long i = 0; i < height; ++i) {
		for (long j = 0; j < width; ++j) {
			if (fabs(arr[pmin_w[j] * width + j] - arr[i * width + pmax_h[i]]) < EXP) {
				if (!flagOf1tAnchor) {
					printf(", ");
				}
				flagOf1tAnchor = 0;
				printf("(%ld, %ld)", i, j);
			}
		}
	}

	free(pmin_w);
	free(pmax_h);
}

void print_matrix(double *arr, long width, long height)
{
	printf("Matrix: \n");
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			printf("%.2f, ", arr[i * width + j]);
		}
		printf("\n");
	}
}

enum Errors solve_second_tasck() {
	long height, width;
	double *arr;
	enum Errors err = CONTINUE;
	printf("Input matrix width: ");
	err = read_in_buffer();
	if (err != CONTINUE)
		return err;
	err = get_answer_from_buffer(buffer, &width);
	if (err != CONTINUE)
		return err;


	printf("Input matrix height: ");
	err = read_in_buffer();
	if (err != CONTINUE)
		return err;
	err = get_answer_from_buffer(buffer, &height);
	if (err != CONTINUE)
		return err;

	arr = (double *) calloc(height * width, sizeof(double));

	get_valid_array(height * width, arr);

	print_matrix(arr, width, height);

	double sum = 0, tmpSum = 0;
	int flag = 0;
	for (int i = 0; i < height; ++i) {
		tmpSum = 0;
		flag = 0;
		for (int j = 0; j < width; ++j) {
			tmpSum += arr[i * width + j];
			if (arr[i * width + j] < 0) {
				flag = 1;
			}
		}
		if (flag) {
			sum += tmpSum;
		}
	}
	printf("Sum: %.3lf\n", sum);

	printf("Anchor points: [");
	print_anchor_points(arr, width, height);
	printf("]\n");

	free(arr);
	return CONTINUE;
}

enum Errors solve_first_tasck(struct node_t **root) {
	enum Errors err = CORRECT;

	err = read_in_buffer();
	if (err != CONTINUE)
		return err;

	double key = NAN;
	double value = NAN;
	for (int i = 0; i < NUM_OF_VALID_COMMANDS; ++i) {
		if (strncmp(buffer, valid_commands[i], strlen(valid_commands[i])) == 0) {
			switch (i) {
				case 0://add
					err = get_valid_argument(&buffer[strlen(valid_commands[i])], 2, &key, &value);
					if (err != CONTINUE)
						return err;

					*root = add_tree(*root, key, value);

					printf("[(");
					tree_print(*root);
					printf(")]");

					return CONTINUE;
				case 1://find
					err = get_valid_argument(&buffer[strlen(valid_commands[i])], 1, &key);
					if (err != CONTINUE)
						return err;

					value = find_by_key(*root, key);
					if (isnan(value)) {
						return EXIST;
					}
					printf("Key %.3f successfully found! Value is %.3f", key, value);
					return CONTINUE;
				case 2://done
					return CORRECT;
				case 3:
				case 4://BinaryTree
					printf("[(");
					tree_print(*root);
					printf(")]");

					return CONTINUE;
				default:
					break;
			}
			break;
		}
	}

	return INVALID;
}

int main(int argc, char const *argv[]) {
	printf("Dulfilled Alexey Dunaev, P3117; 6 option\n");

	enum States state = START;
	long readAns = -1;
	enum Errors err;

	printf("Please choose part of task\n");
	err = read_in_buffer();
	if (err == CONTINUE)
		err = get_answer_from_buffer(buffer, &readAns);

	if (err != CONTINUE || !(readAns == 1 || readAns == 2)) {
		printf("\nInvalid part number, type '1' or '2'\n");
		state = EXIT;
	} else {
		state = (enum States) readAns;
	}

	struct node_t *root;
	switch (state) {
		case FIRST:
			root = NULL;
			printf("Part 1 - Implemented work with a binary tree. "
					       "Valid commands are: "
					       "add {key} {value}; "
					       "find {key}; "
					       "done - completes the demonstration; "
					       "BinaryTree [({key}, {value}, ({left_node}), ({right_node}))] - print a tree.\n");

			do {
				printf(">>> ");
				err = solve_first_tasck(&root);
				say_error(err);
			} while (err);

			free_mem(root);
			break;

		case SECOND:
			printf("Part 2 - Implemented work with matrix.\n");

			err = solve_second_tasck();
			if (err != CONTINUE) {
				exit(1);
			}
			break;

		default:
			exit(1);
	}
	printf("Bye!\n");
	return 0;
}
