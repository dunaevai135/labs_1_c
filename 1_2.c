#include <stdio.h>
#include <math.h>
#include <string.h>

enum States {
	START,
	COMPUTE,
	EXIT,
	INCORRECT
};

// 6 variant
double func1(double a)
{
	return cos(a)+cos(2*a)+cos(6*a)+cos(7*a);
}
double func2(double a)
{
	return 4*cos(a/2)*cos(5/2*a)*cos(4*a);
}

void flush_input(void) {
    char c;
    while ( scanf("%c", &c) == 1 && c != '\n' );
}

int main()
{
	enum States state = START;
	double readDouble;
	char readChar;
	int err = 0;
	char nextChar;
	while (state != EXIT)
	{
		switch(state)
		{
			case START:
				printf("1. Compute a function\n");
				printf("2. Exit\n");
				printf("Choose a menu item: ");

				readChar = getchar();
				
				if (!strchr ("12", readChar) || !(nextChar == '\n' || nextChar == '\r'))
				{
					printf("\nPlease, choose a correct menu item:\n");
					break;
				}
				state = readChar - '0';
				break;
			case COMPUTE:
				printf("Z1 = cos(a)+cos(2*a)+cos(6*a)+cos(7*a)\n");
				printf("Z2 = 4*cos(a/2)*cos(5/2*a)*cos(4*a)\n");
				printf("Input argument A: ");

				err = scanf("%lf", &readDouble);
				nextChar = getchar();
				
				if ('\n' != nextChar)
				{
					flush_input();
					err = -1;
				}

				if (err < 1 || isinf(readDouble) || isnan(readDouble))
				{
					state = INCORRECT;
					printf("\n");
					break;
				}
				printf("Z1: %.3f\n", func1(readDouble));
				printf("Z2: %.3f\n", func2(readDouble));
				printf("\n");
				state = START;
				break;

			case INCORRECT:
				printf("Wrong input argument X. Press `y` to continue, another key to return back:\n");
				readChar = getchar();
				
				if ('\n' != readChar)
				{
					flush_input();
				}
				if (!strchr ("yY", readChar))
				{
					state = START;
					printf("\n");
					break;
				}

				state = COMPUTE;
				break;

			default:
				state = EXIT;
		}
	}
	return 0;
}