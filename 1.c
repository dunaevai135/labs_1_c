#include <stdio.h>
#include <math.h>

enum States {
	START,
	COMPUTE,
	EXIT,
	INCORRECT
};

// 6 variant
double func1(double a)
{
	return cos(a)+cos(2*a)+cos(6*a)+cos(7*a);
}
double func2(double a)
{
	return 4*cos(a/2)*cos(5/2*a)*cos(4*a);
}

void flush_input(void) {
	char c;
	while ( scanf("%c", &c) == 1 && c != '\n' );
}

int main(int argc, char const *argv[])
{
	int state = START;
	int readInt;
	double readDouble;
	char readChar;
	int err = 0;
	while (state != EXIT)
	{
		switch(state)
		{
			case START:
				printf("1. Compute a function\n");
				printf("2. Exit\n");
				printf("Choose a menu item: ");
				err = scanf("%i", &readInt);

				if (err < 1 || readInt < 1 || readInt > 2)
				{
					flush_input();
					printf("\n\nPlease, choose a correct menu item:\n");
					break;
				}

				state = readInt;
				break;
			case COMPUTE:
				printf("Input argument X: ");
				err = scanf("%lf", &readDouble);
				printf("\n");
				if (err < 1)
				{
					flush_input();
					state = INCORRECT;
					printf("\n");
					break;
				}
				printf("Z1: %.3f\n", func1(readDouble));
				printf("Z2: %.3f\n", func2(readDouble));
				printf("\n");
				state = START;

				break;

			case INCORRECT:
				printf("Wrong input argument X. Press `y` to continue, another key to return back:\n");
				readChar = getchar();

				if (readChar != 'y')
				{
					flush_input();
					printf("\n");
					state = START;
					break;
				}
				state = COMPUTE;
				break;

			default:
				state = EXIT;
				break;
		}
	}
	return 0;
}