//
// Created by dunaev on 27.09.17.
//

#include <stdio.h>
#include <ctype.h>
#include <errno.h>

/**
 * Check char on equality sentence break
 * @param a
 * @return
 */
int issentencebreak(int a) {
	if (a == '.' || a == '?' || a == '!')
		return 1;
	return 0;
}

/**
 * check file on sentence end
 * @param a
 * @param file
 * @return
 */
int check_file_on_sentence_end(int a, FILE *file) {
	if (issentencebreak(a)) {
		a = fgetc(file);
		if (isspace(a) || a == EOF) {
			return 1;
		} else {
			fseek(file, -1, SEEK_CUR);
		}
	}
	return 0;
}

/**
 * Copy sentence from file in position to stdout
 * @param file variable
 * @param pos - position
 * @return 1 if file read error
 */
int fput_sentence_pos(FILE *file, fpos_t pos) {
	fsetpos(file, &pos);
	int a = fgetc(file);
	if (isspace(a)) {
		a = fgetc(file);
	}
	while (!check_file_on_sentence_end(a, file)) {
		if (ferror(file)) {
			return 1;
		}
		if (a == '\n') {
			putchar(' ');
		} else {
			putchar(a);
		}
		a = fgetc(file);
	}
	putchar(a);
	putchar('\n');
	return 0;
}

/**
 * Split on sentence, delete sentence with comma, put in stdout
 * @param file
 */
void put_splitted_sentence_nocomma(FILE *file) {
	int a = 1, flag_to_getpos = 0, comma_flag = 0;
	fpos_t pos;
	fgetpos(file, &pos);

	while ((a = fgetc(file)) != EOF) {
		if (a == ',') {
			comma_flag = 1;
		}
		if (check_file_on_sentence_end(a, file)) {
			if (!comma_flag) {
				if (fput_sentence_pos(file, pos)) {
					break;
				}
			}
			comma_flag = 0;
			flag_to_getpos = 1;
		}
		if (flag_to_getpos) {
			fgetpos(file, &pos);
			if (ferror(file)) {
				break;
			}
			flag_to_getpos = 0;
		}
	}

}

/**
 * Entry point
 * @return 0 - correct
 * 1 - no arg
 * 3 - file not found
 * 4 - can`t be read
 */
int main(int argc, char const *argv[]) {
	if (argc <= 1) {
		printf("Please provide the name of file.\n");
		return 1;
	}

	if (argc > 2) {
		printf("Wrong arguments for application. Too many args.\n");
		return 2;
	}

	FILE *file;
	file = fopen(argv[1], "r");

	if (file == NULL) {
		switch (errno) {
			case EACCES:
				printf("File “%s” can’t be read. Permission denied\n", argv[1]);
				return 4;
			case ENOENT:
				printf("File “%s” not found. File does not exist\n", argv[1]);
				return 3;
			case ETXTBSY:
				printf("File “%s” busy\n", argv[1]);
				return 4;
			case EMFILE:
				printf("Too many open files \n");
				return 4;
			default:
				printf("File “%s” unexpected error\n", argv[1]);
				return 4;
		}
	}

	put_splitted_sentence_nocomma(file);

	if (ferror(file)) {
		printf("File “%s” can’t be read.\n", argv[1]);
		fclose(file);
		return 4;
	}

	fclose(file);

	return 0;
}